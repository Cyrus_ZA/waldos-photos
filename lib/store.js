import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { reducer as formReducer } from 'redux-form'
import thunkMiddleware from 'redux-thunk'
import uuid from "uuid";

const cartInitialState = [];

export const actionTypes = {
    ADD: 'ADD',
    REMOVE: 'REMOVE',
}

export const cartReducer = (state = cartInitialState, action) => {
    switch (action.type) {
        case actionTypes.ADD:
            return [...state, action.payload]
        case actionTypes.REMOVE:
            const index = state.findIndex(o => o.id === action.payload)
            return [
                ...state.slice(0, index),
                ...state.slice(index + 1)
            ]
        default:
            return state
    }
}

const reducers = combineReducers({
    form: formReducer,
    cart: cartReducer
})

export const addToCart = pizza => dispatch => {
    const {size, toppings, price} = pizza;
    const payload = {
        id: uuid(),
        size,
        toppings,
        price,
    }
    return dispatch({
        type: actionTypes.ADD,
        payload
    })
}
export const removeFromCart = id => dispatch => {
    return dispatch({
        type: actionTypes.REMOVE,
        payload: id
    })
}

export const initStore = (initialState = {}) => {
    return createStore(reducers, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)))
}