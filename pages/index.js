import React from 'react'
import { initStore, } from '../lib/store'
import withRedux from 'next-redux-wrapper'

import Layout from '../components/Layout'
import Menu from '../components/Menu'
import withApollo from '../lib/withApollo'
import Cart from "../components/Cart";

class Index extends React.Component {
    static getInitialProps({isServer}) {
        return {isServer}
    }

    render() {
        return (
            <Layout>
                <Cart/>
                <Menu/>
            </Layout>
        )
    }
}

export default withRedux(initStore)(
    withApollo(Index)
)
