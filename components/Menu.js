import { Component } from 'react';
import MenuForm from "./MenuForm";
import ErrorMessage from "./ErrorMessage";
import { compose, graphql } from "react-apollo";
import { gql } from "apollo-client-preset";
import { addToCart } from "../lib/store";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class Menu extends Component {
    state = {selectedIndex: 0}
    addPizza = ({toppings, basePrice, size}) => {
        const selectedToppings = toppings.filter(o => o.selected)
        const pizza = {
            toppings: selectedToppings.map(o => o.name),
            price: basePrice + selectedToppings.length && selectedToppings.map(o => o.price).reduce((acc, val) => val + acc),
            size
        }
        this.props.addToCart(pizza)
    }

    selectSize(e) {
        const selectedIndex = this.props.data.pizzaSizes.findIndex(o => o.name === e.target.value);
        this.setState({selectedIndex})
    }


    render() {
        const {error, data} = this.props;
        if (data.loading) return 'Loading...';
        if (error || data.error || !data.pizzaSizes) return <ErrorMessage message='Error loading pizzas.'/>

        const {name, maxToppings, basePrice, toppings = []} = data.pizzaSizes[this.state.selectedIndex];
        const initialValues = {
            size: name,
            toppings: toppings
                .map(({topping: {name, price}, defaultSelected}) => ({
                    name,
                    price,
                    selected: defaultSelected
                }))
        }

        return (
            <div>
                <h2>Menu</h2>
                <MenuForm
                    onSizeChange={this.selectSize.bind(this)}
                    sizes={data.pizzaSizes}
                    basePrice={basePrice}
                    maxToppings={maxToppings}
                    onSubmit={({toppings, size}) => {
                        this.addPizza({
                            toppings,
                            size,
                            basePrice
                        })
                    }}
                    initialValues={initialValues}
                />
            </div>
        )
    }
}

const query = gql`
    query PizzaSizes {
        pizzaSizes {
            name
            maxToppings
            basePrice
            toppings {
                defaultSelected
                topping {
                    name
                    price
                }
            }
        }
    }
`

const mapStateToProps = ({cart}) => ({cart})

const mapDispatchToProps = dispatch => {
    return {
        addToCart: bindActionCreators(addToCart, dispatch),
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    graphql(query)
)(Menu)
