import { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { removeFromCart } from "../lib/store";
import Pizza from "./Pizza";
import Currency from "./Currency";

class Menu extends Component {
    removePizza = (id) => {
        this.props.removeFromCart(id)
    }

    render() {
        const {cart} = this.props;
        const totalCost = cart.length && cart.map(o => o.price).reduce((acc, val) => acc + val)
        return (
            <div>
                <h2>Cart:</h2>
                <div>
                    {!cart.length && 'Cart is empty'}
                </div>
                <ul>
                    {cart.map(o =>
                        <li key={o.id}>
                            <Pizza {...o} />
                            <button onClick={() => this.removePizza(o.id)}>Remove</button>
                        </li>
                    )}
                </ul>
                <b>Total Cost: <Currency value={totalCost}/></b>

            </div>
        )
    }
}

const mapStateToProps = ({cart}) => ({cart})

const mapDispatchToProps = dispatch => {
    return {
        removeFromCart: bindActionCreators(removeFromCart, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu)
