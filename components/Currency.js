import currencyFormatter from "currency-formatter";

const Currency = ({value}) =>
    <span>{currencyFormatter.format(value, {code: 'USD'})}</span>

export default Currency;