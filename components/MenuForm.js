import React, { Component } from 'react'
import { Field, FieldArray, Form, formValueSelector, reduxForm } from 'redux-form'
import { connect } from "react-redux";
import Currency from "./Currency";
import ErrorMessage from "./ErrorMessage";

const renderToppings = ({fields, meta: {error}}) => (
    <div>
        {error && <ErrorMessage message={error}/>}
        <ul>

            {fields.map((topping, index) => (
                <li key={index}>
                    <div>
                        <div>
                            <Field
                                name={`${topping}.selected`}
                                component="input"
                                type="checkbox"
                            />
                            <Field
                                disabled={true}
                                name={`${topping}.name`}
                                component={({input}) =>
                                    <span>{input.value}</span>}
                            />
                            <Field
                                disabled={true}
                                name={`${topping}.price`}
                                component={({input}) => <Currency value={input.value}/>}
                            />
                        </div>
                    </div>
                </li>
            ))}
        </ul>
    </div>
)

class MenuForm extends Component {
    render() {
        const {handleSubmit, onSizeChange, sizes, basePrice, toppingCosts, invalid} = this.props
        return (
            <Form onSubmit={handleSubmit}>
                <div>
                    <label>Size</label>
                    <div>
                        <Field onChange={onSizeChange} name="size" component="select">
                            {sizes.map(o =>
                                <option key={o.name} value={o.name}>{o.name}</option>
                            )}
                        </Field>
                    </div>
                </div>

                <div>Price: <Currency value={toppingCosts + basePrice}/></div>
                <button disabled={invalid} type="submit">Add To Card</button>
                <FieldArray validate={validate} name="toppings" component={renderToppings}/>
            </Form>
        )
    }
}


const validate = (toppings = [], {}, {maxToppings}) => {
    const maxToppingsReached = maxToppings && maxToppings < toppings
        .filter(o => o.selected).length;

    if (maxToppingsReached)
        return "Too many toppings selected";
}


const selector = formValueSelector('MenuForm')

const mapStateToProps = (state) => {
    const toppings = selector(state, 'toppings') || [];
    const selectedToppings = toppings
        .filter(o => o.selected);
    if (!selectedToppings) return {}
    const toppingCosts = selectedToppings.length && selectedToppings
        .map(o => o.price)
        .reduce((acc, val) => val + acc)
    return {toppingCosts}
}
export default connect(mapStateToProps)(reduxForm({form: 'MenuForm', enableReinitialize: true})(MenuForm))