import Currency from "./Currency";

export default ({size, price, toppings}) =>
    <div>
        <div><b>Size: {size}</b></div>
        <div>Price: <Currency value={price}/></div>
        <div>Toppings: {toppings.join(', ')}</div>
    </div>